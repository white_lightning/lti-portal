package controllers

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.i18n.{Messages, MessagesApi, I18nSupport}
import play.api.Logger

import models.{Session}
import reactivemongo.bson.BSONObjectID

class PortalController @Inject()(val messagesApi:MessagesApi) extends Controller with I18nSupport  {

  def index(path: String = "") = Action.async { implicit request =>
    Session.auth{ session =>
      path match {
        case p: String if p.length > 0 =>
          Logger.info("Session found - redirecting to index for Portal.")
          Future(Redirect("/portal"))
        case p =>
          Logger.info("Session found - sending to index for Portal.")
          Future(Ok(views.html.portal())
            .withHeaders("X-FRAME-OPTIONS" -> ("ALLOW-FROM " + Application.canvasBase))
            .withHeaders(CACHE_CONTROL -> { "no-cache, no-store, must-revalidate" })
            .withHeaders(PRAGMA -> { "no-cache" })
            .withHeaders(EXPIRES -> { "-1" }))
      }
    }    
  }
}

