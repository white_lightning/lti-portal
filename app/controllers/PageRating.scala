package controllers

import javax.inject._

import play.api.Logger
import play.api.libs.json._
import play.api.mvc._

import play.modules.reactivemongo._
import models.PageRating

import reactivemongo.api.ReadPreference
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.play.json._
import reactivemongo.play.json.collection._
import reactivemongo.bson.BSONDocument

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class PageRatingController @Inject() (val reactiveMongoApi: ReactiveMongoApi) 
      extends Controller with MongoController with ReactiveMongoComponents {
    
    def pageRatingFuture: Future[JSONCollection] = database.map(_.collection[JSONCollection]("pageRating"))
    
    // def index = Action.async {
    //     val futurePageRatingList: Future[List[PageRating]] = pageRatingFuture.flatMap {
    //         _.find(Json.obj()).
    //         cursor[PageRating](ReadPreference.primary).
    //         collect[List](5)
    //     }
    //     futurePageRatingList.map{ ps =>
    //         Ok(views.html.index(ps))
    //     }
    // }
    def main = Action.async {
        val futurePageRatingList: Future[List[PageRating]] = pageRatingFuture.flatMap {
            _.find(Json.obj())
            .cursor[PageRating](ReadPreference.primary)
            .collect[List](15)
        }
        futurePageRatingList.map{ ps =>
            Ok(views.html.main(ps))
        }
    }
    
    def formPost = Action.async { request =>
        val bodyForm: Map[String, String] = request.body.asFormUrlEncoded.get.map{ case(k, v) =>
            k -> v.mkString
        }
        val newRating = PageRating(bodyForm.getOrElse("rating", ""))
        Logger.info(s"new rating ${newRating}")
        for {
            ratings <- pageRatingFuture
            lastError <- ratings.insert(newRating)
        } yield {
            Logger.debug(s"Successfully inserted with LastError: $lastError")
            Created("WOO Hoo")
        }

        Logger.info(s"$newRating")
        Future(Redirect("/main"))
    }

    def xhrPost = Action.async(parse.json) { request =>
        Json.fromJson[PageRating](request.body) match {
            case JsSuccess(newRating, _) =>
                for {
                    pageRatings <- pageRatingFuture
                    lastError <- pageRatings.insert(newRating)
                } yield{
                    Logger.debug(s"Successfully inserted with LastError: $lastError")
                    Created("Created 1 Rating")
                }
            case JsError(errors) =>
                Future.successful(BadRequest(s"Could not build a city from the json provided. $errors"))
        }
    }
}