/**
 * Author  Lyle Frost <lfrost@everspringpartners.com>
 */

package controllers

import javax.inject.Inject

import org.joda.time.DateTime
import org.joda.time.DateTimeZone.UTC

import scala.concurrent.{ExecutionContext, Future}
import scala.concurrent.duration._

import play.api.i18n.Messages
import play.api.i18n.{MessagesApi, I18nSupport}
import play.api.Logger
import play.api.mvc.{Action, Controller}
import play.api.Play.current
import play.modules.reactivemongo.{
  MongoController,
  ReactiveMongoApi,
  ReactiveMongoComponents
}

import akka.actor.ActorSystem

import reactivemongo.bson.BSONObjectID

import models.Session
// import models.services.lti.request.{Request => LTIRequest}
// import models.services.lti.request.params._
// import models.services.PortfolioViewCacher

class Application @Inject()(actor: ActorSystem) (val messagesApi:MessagesApi, 
    val reactiveMongoApi: ReactiveMongoApi)(implicit ec:ExecutionContext) 
    extends Controller with I18nSupport with MongoController with ReactiveMongoComponents {
  
    Logger.info("Application initializing.")

  Application.mongoDb = Some(reactiveMongoApi.db)

    def ltiAuthenticate(application:String, moduleItemId:Long = 0) = Action.async { implicit request =>
        Logger.debug("request.path = " + request.path)
        Logger.debug("request.method = " + request.method)
        val params = request.body.asFormUrlEncoded.get
        val sessionId = BSONObjectID.generate
        val session = Session(
            _id = Some(sessionId)
        ,   remoteAddress = Some(request.remoteAddress)
        ,   application = Some(application)
        ,   userId = Some(params.getOrElse("custom_canvas_user_id", List("0")).head.toLong)
        ,   ltiUserId = Some(params.getOrElse("user_id", List("")).head)
        ,   userNameFirst = Some(params.getOrElse("lis_person_name_given", List("")).head)
        ,   userNameLast = Some(params.getOrElse("lis_person_name_family", List("")).head)
        ,   avatar = Some(params.getOrElse("user_image", List("")).head)
        ,   externalRoles = Some(params.getOrElse("ext_roles", List("")).head.split(",").toList)
        ,   currentRoles = Some(params.getOrElse("roles", List("")).head.split(",").toList)
        // ,   request = Some(LTIRequest.fromRequest(params))
        )

        Session.insert(session) map {
            n =>
                Logger.debug("Session custom_canvas_user_id " + session.userId)
                Logger.debug("Session roles " + session.currentRoles)
                // Logger.debug("Session email " + session
                  // .request.getOrElse(LTIRequest())
                  // .toolConsumerParams.getOrElse(ToolConsumer())
                  // .contactEmail.getOrElse("")
                // )
                Logger.debug("Session record created with BSON ID " + sessionId.stringify + ".")
                session.application match {
                    case Some("portal")              => Redirect(routes.PortalController.index).withSession("sessionId" -> sessionId.stringify)
                    case _                           => NotFound(<h1>Application not found</h1>)
                }
        }
    }
  
  def initx() : Unit = {
    mongoTestConnection()
  }

  private def mongoTestConnection() : Unit = {
    import reactivemongo.api.BSONSerializationPack
    import reactivemongo.api.commands.Command
    import reactivemongo.bson.BSONDocument

    val buildInfo = BSONDocument("buildInfo" -> 1)
    val runner = Command.run(BSONSerializationPack)
    val futureResult = runner.apply(Application.mongoDb.get, runner.rawCommand(buildInfo)).one[BSONDocument]
    futureResult.map(result => Logger.info("MongoDB version: " + result.getAs[String]("version").get))
  }
}

object Application {
  var mongoDb:Option[reactivemongo.api.DefaultDB] = None
  val applicationName = current.configuration.getString("application.name").getOrElse("")
  val canvasBase = current.configuration.getString("app.canvasBase").getOrElse("")
}
