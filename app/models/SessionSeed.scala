package models.seeds

import models.Session
import reactivemongo.bson.BSONObjectID

trait SessionSeeds {
    def seed: Session = Session(
        _id                     = Some(BSONObjectID("5742d50f20f7e80b0bd9f9af865aebfe50545afc"))
    // ,   whenCreated             = Some(DateTime.now(UTC))
    // ,   whenUpdated             = Some(DateTime.now(UTC))
    ,   remoteAddress           = Some("0:0:0:0:0:0:0:1")
    ,   application             = Some("portal")
    ,   userId                  = Some(145)
    ,   ltiUserId               = Some("145")
    ,   userNameFirst           = Some("Matthew")
    ,   userNameLast            = Some("White")
    ,   avatar                  = Some("https://secure.gravatar.com/avatar/8b3c8ede828a507d6afb5fa7c8a6743e?s=128&d=identicon")
    ,   externalRoles           = Some(List("urn:lti:instrole:ims/lis/Student"))
    ,   currentRoles            = Some(List("urn:lti:instrole:ims/lis/Administrator"))
    )
}


