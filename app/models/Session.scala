package models

import seeds.SessionSeeds

import org.joda.time.DateTime
import org.joda.time.DateTimeZone.UTC

import scala.concurrent.Future
import scala.util.{Failure, Success}

import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.Logger
import play.api.Play.current
import play.api.mvc._

// import services.lti.request.{Request => LTIRequest}

// import utils.serializers.BSONSerializer.{
//     BSONMap,
//     BSONDateTimeHandler
// }

import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.{
    BSONString,
    BSONDateTime,
    BSONDocument,
    BSONDocumentReader,
    BSONDocumentWriter,
    BSONObjectID,
    Macros
}

case class Session(
    _id                     : Option[BSONObjectID] = None
// ,   whenCreated             : Option[DateTime] = Some(DateTime.now(UTC))
// ,   whenUpdated             : Option[DateTime] = None
,   remoteAddress           : Option[String] = None
,   application             : Option[String] = None
,   userId                  : Option[Long] = None
,   ltiUserId               : Option[String] = None
,   userNameFirst           : Option[String] = None
,   userNameLast            : Option[String] = None
,   avatar                  : Option[String] = None
,   externalRoles           : Option[List[String]] = None
,   currentRoles            : Option[List[String]] = None
)


object Session extends SessionSeeds {

    implicit val sessionBSONHandler = Macros.handler[Session]

    val app = play.core.ApplicationProvider

    private val collectionName : String = current.configuration.getString("app.collections.session") match {
      case Some(s) => s
      case None    => {
        Logger.error("app.collections.session missing from application configuration.")
        "unknown"
      }
    }

    private val DEV: Boolean = current.configuration.getBoolean("app.is-dev") match {
      case Some(s) => s
      case None => false
    }

    private val collection : BSONCollection = controllers.Application.mongoDb.get.collection[BSONCollection](collectionName)

    def auth(f: Session => Future[Result])(implicit request: Request[Any]) = for {
        session <- findCurrent()
        result <- session match {
            case Some(s) => f(s)
            case s if DEV => f(seed)
            case None => Future(Results.Forbidden)
        }
    } yield result
    
    /** Retrieve the Session record for the current user session.
      *
      * @param request  The implicit HTTP request
      */
    def findCurrent()(implicit request:play.api.mvc.Request[Any]) : Future[Option[Session]] = {
        request.session.get("sessionId") match {
            case s if DEV => Future(Some(seed))
            case Some(id) => {
                val selector = BSONDocument(
                    "_id" -> BSONObjectID(id)
                )
                collection.find(selector).one[Session]
            }
            case None => {
                Future {
                    Logger.error("HTTP request has no sessionId value.  Complete session = " + request.session + ".")
                    None
                }
            }
        }
    }

    def find(id:String) : Future[List[Session]] = {
        val selector = BSONDocument(
            "_id" -> BSONObjectID(id)
        )

        collection.find(selector).cursor[Session]().collect[List]()
    }

    def insert(session:Session) : Future[WriteResult] = {
        val futureInsert = collection.insert(session)

        futureInsert.onComplete {
            case Failure(e) => {
                Logger.error("Error inserting session document ID " + session._id.get.stringify + ": " + e.printStackTrace)
            }
            case Success(lastError) => {
                Logger.info("Inserted session document ID " + session._id.get.stringify + ": " + lastError)
            }
        }

        futureInsert
    }

    def touch(id:String) {
        val selector = BSONDocument("_id" -> BSONObjectID(id))
        val modifier = BSONDocument(
            "$set" -> BSONDocument(
                "whenUpdated" -> BSONDateTime(DateTime.now(UTC).getMillis)
            )
        )
        val future = collection.update(selector, modifier, multi = false)

        future.onComplete {
            case Failure(e) => {
                Logger.error("Error touching session document ID " + id + ": " + e.printStackTrace)
            }
            case Success(lastError) => {
                Logger.info("Touched session document ID " + id + ": " + lastError)
            }
        }
    }
}