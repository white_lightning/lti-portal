package models

import play.api.libs.json.Json

case class PageRating (
    // _id: BSONPObjectID
    // createdAt: 
    rating: String
// ,   comment: Option[String]
// ,   canvasUser: Option[CanvasUser]
// ,   canvasUserId: Option[Long]
// ,   canvasUserName: Option[String]
// ,   canvasUserAvatarUrl: Option[String]
)

object PageRating {
  implicit val formatter = Json.format[PageRating]

  // def insertOne(coll: BSONCollection, doc: BSONDocument): Future[Unit] = {
  //   val writeRes: Future[WriteResult] = coll.insert(document1)

  //   writeRes.onComplete { // Dummy callbacks
  //     case Failure(e) => e.printStackTrace()
  //     case Success(writeResult) =>
  //       println(s"successfully inserted document with result: $writeResult")
  //   }

  //   writeRes.map(_ => {}) // in this example, do nothing with the success
  // }
}